# AI NOTEBOOKS

![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg) [![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/) [![GPL 3 Licence](https://badges.frapsoft.com/os/gpl/gpl.svg?v=103)](https://opensource.org/licenses/GPL-3.0/)



Copyright (c) 2021 - present, [Sanat Gupta](https://sanatg.github.io)

This is the code so far done in AI classes.

## Requirements

there are nothing special requirements just python3 and jupyter notebook.

## License

[![GPL](https://licensebuttons.net/l/GPL/2.0/88x62.png)](./LICENSE.md)

Copyright (c) 2021 - present, [Sanat Gupta](https://sanatg.github.io)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.